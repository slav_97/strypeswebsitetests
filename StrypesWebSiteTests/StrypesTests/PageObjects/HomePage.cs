﻿using System;
using System.Linq;
using OpenQA.Selenium;
using System.Threading;

namespace StrypesTests.PageObjects
{
    public partial class HomePage
    {
        IWebDriver driver;

        public HomePage(IWebDriver driver)
        {
            this.driver = driver;
        }

        public void goToPage()
        {
            driver.Navigate().GoToUrl("https://strypes.eu");
        }

        public void clickHomeButton()
        {
            driver.FindElement(home).Click();
        }

        public void clickStrypesLogo()
        {
            driver.FindElement(logo).Click();        
        }

        public void goToAboutUs()
        {
            driver.FindElement(about).Click();
            driver.FindElement(aboutUs).Click();
        }

        public void goToOurClients()
        {
            driver.FindElement(about).Click();
            driver.FindElement(ourClients).Click();
        }

        public void goToOurPartners()
        {
            driver.FindElement(about).Click();
            driver.FindElement(ourPartners).Click();
        }

        public void goToOurLeadersip()
        {
            driver.FindElement(about).Click();
            driver.FindElement(ourLeadership).Click();
        }

        public void goToOurServices()
        {
            driver.FindElement(services).Click();
            driver.FindElement(ourServices).Click();
        }

        public void goToOurWorkModel()
        {
            driver.FindElement(services).Click();
            driver.FindElement(ourWorkModel).Click();
        }

        public void goToVacancies()
        {
            driver.FindElement(careers).Click();
            driver.FindElement(vacancies).Click();
        }

        public void goToWorkingAtStrypes()
        {
            driver.FindElement(careers).Click();
            driver.FindElement(workingAtStrypes).Click();
        }

        public void goToInternship()
        {
            driver.FindElement(careers).Click();
            driver.FindElement(internship).Click();
        }

        public void goToBlog() 
        {
            driver.FindElement(resources).Click();
            driver.FindElement(blog).Click();
        }

        public void goToWhitepapers()
        {
            driver.FindElement(resources).Click();
            driver.FindElement(whitepapers).Click();
        }
        public void goToVideos()
        {
            driver.FindElement(resources).Click();
            driver.FindElement(videos).Click();
        }

        public void goToMediaKit()
        {
            driver.FindElement(resources).Click();
            driver.FindElement(mediaKit).Click();
        }

        public void goToContacts()
        {
            driver.FindElement(contacts).Click();
        }

        public void GoToConsultancy() 
        {
            IWebElement element = driver.FindElement(consultancy);
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].scrollIntoView(true);", element);
            Thread.Sleep(4000);
            ((IJavaScriptExecutor)driver).ExecuteScript("window.scrollBy(0,-100)");
            Thread.Sleep(1000);
            driver.FindElement(consultancy).Click();
        }

        public void goToHeaderNearsurance()
        {
            driver.FindElement(headerNearsurane).Click();
        }

        public void goToContentNearsurance()
        {
            IWebElement element = driver.FindElement(contentNearsurance);
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].scrollIntoView(true);", element);
            Thread.Sleep(5000);
            ((IJavaScriptExecutor)driver).ExecuteScript("window.scrollBy(0,-100)");
            Thread.Sleep(100);
            driver.FindElement(contentNearsurance).Click();
            Thread.Sleep(5000);
        }

        public void clickSubscribe()
        {
            driver.FindElement(subscribe).Click();
        }

        public void clickGetYourFreeCaseStudy()
        {
            driver.FindElement(getFreeCaseStudy).Click();
        }

        public void goToDigitalTransformation()
        {
            IWebElement element = driver.FindElement(digitalTransoformation);
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].scrollIntoView(true);", element);
            Thread.Sleep(4000);
            ((IJavaScriptExecutor)driver).ExecuteScript("window.scrollBy(0,-100)");
            Thread.Sleep(1000);
            driver.FindElement(digitalTransoformation).Click();
        }

        public void goToApplicationDevelopmentModernizationManagement()
        {
            IWebElement element = driver.FindElement(applicationDevelopmentModernizationManagement);
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].scrollIntoView(true);", element);
            Thread.Sleep(4000);
            ((IJavaScriptExecutor)driver).ExecuteScript("window.scrollBy(0,-100)");
            Thread.Sleep(1000);
            driver.FindElement(applicationDevelopmentModernizationManagement).Click();
        }

        public void goToItInfrastructure()
        {
            IWebElement element = driver.FindElement(ITInfrastructure);
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].scrollIntoView(true);", element);
            Thread.Sleep(4000);
            ((IJavaScriptExecutor)driver).ExecuteScript("window.scrollBy(0,-100)");
            Thread.Sleep(1000);
            driver.FindElement(ITInfrastructure).Click();
        }

        public void scrollToCarousel()
        {
            IWebElement element = driver.FindElement(By.XPath("/html/body/div[3]/section[8]/div/div[2]/div/div"));
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].scrollIntoView(true);", element);
            Thread.Sleep(4000);
            ((IJavaScriptExecutor)driver).ExecuteScript("window.scrollBy(0,-100)");
            Thread.Sleep(100);
        }
        public int getCarouselActiveItem()
        {
            scrollToCarousel();

            var carouselContainer = driver.FindElement(By.XPath("/html/body/div[3]/section[8]/div/div[2]/div/div"));
            var items = carouselContainer.FindElements(By.XPath("*"));

            for (int i = 0; i < 4; i++)
            {
                if (items[i].GetAttribute("class") == "item active") return i + 1;
                //Console.WriteLine(items[i].GetAttribute("class"));
            }

            return -1;
        }

        public void switchCarouselItem(int carouselIndex)
        {
            if (carouselIndex < 1 || carouselIndex > 4)
            {
                throw new ArgumentOutOfRangeException("Valid carousel indexes are from 1 to 4!");
            }
            driver.FindElement(By.XPath("/html/body/div[3]/section[8]/div/div[2]/div/ol/li[" + carouselIndex + "]")).Click();
        }

        public void scrollDownToSomeOfOurClients()
        {
            IWebElement element = driver.FindElement(SomeOfOurClients);
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].scrollIntoView(true);", element);
            Thread.Sleep(4000);
            ((IJavaScriptExecutor)driver).ExecuteScript("window.scrollBy(0,-100)");
            Thread.Sleep(100);
        }

        public void pressTheJoinTheTeamButton()
        {
            IWebElement element = driver.FindElement(JoinTheTeamButton);
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].scrollIntoView(true);", element);
            Thread.Sleep(4000);
            ((IJavaScriptExecutor)driver).ExecuteScript("window.scrollBy(0,-100)");
            Thread.Sleep(100);
            driver.FindElement(JoinTheTeamButton).Click();
        }

        public void scrollToEmailField()
        {
            IWebElement element = driver.FindElement(subscribeEmailInputField);
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].scrollIntoView(true);", element);
            Thread.Sleep(4000);
            ((IJavaScriptExecutor)driver).ExecuteScript("window.scrollBy(0,-100)");
            Thread.Sleep(100);
        }

        public void enterAnEmailInTheSubscribeInputField(string input)
        {
            driver.FindElement(subscribeEmailInputField).SendKeys(input);
        }

        public void submitSubscibtionEmail()
        {
            driver.FindElement(subscribeSubmitEmailButton).Click();
        }

        public int isRegistrationSuccessful()
        {
            if (driver.FindElements(subscriptionPopUp).Count() < 1)
            {
                return -1;
            }

            string text = driver.FindElement(subscriptionPopUp).Text;

            if (text == "Please enter a valid email.")
            {
                return -1;
            }
            else if (text == "Thanks for subscribing!")
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public void waitForPopUpToDisapear()
        {
            if (driver.FindElements(subscriptionPopUp).Count() > 0)
            {
                Thread.Sleep(100);
                waitForPopUpToDisapear();
            }
        }

        public void clearEmailField()
        {
            driver.FindElement(subscribeEmailInputField).Clear();
        }

        public void scrollToFooter()
        {
            ((IJavaScriptExecutor)driver).ExecuteScript("window.scrollTo(0, document.body.scrollHeight)");
            ((IJavaScriptExecutor)driver).ExecuteScript("window.scrollBy(0,-100)");
            Thread.Sleep(4000);
        }

        public void goToFacebookPage()
        {
            driver.FindElement(facebook).Click();
        }

        public void goToTwitterPage()
        {
            driver.FindElement(twitter).Click();
        }

        public void goToYoutubePage()
        {
            driver.FindElement(youtube).Click();
        }

        public void goToLinkedPage()
        {
            driver.FindElement(linkedin).Click();
        }

        public void acceptCookies()
        {
            driver.FindElement(dismissCookieButton).Click();
        }

        public void useAnchorToTop()
        {
            driver.FindElement(anchorToTop).Click();
        }


    }
}
