﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrypesTests.PageObjects
{
    public partial class HomePage
    {
        private static By logo = By.XPath("/html/body/div[2]/nav/div/div/div[1]/div/a/img[2]");
        private static By home = By.XPath("/html/body/div[2]/nav/div/div/div[2]/div[1]/ul/li[1]/a");
        private static By about = By.XPath("/html/body/div[2]/nav/div/div/div[2]/div[1]/ul/li[2]/span");
        private static By aboutUs = By.XPath("/html/body/div[2]/nav/div/div/div[2]/div[1]/ul/li[2]/div/div/div/div/ul/li[1]/a");
        private static By ourClients = By.XPath("/html/body/div[2]/nav/div/div/div[2]/div[1]/ul/li[2]/div/div/div/div/ul/li[2]/a");
        private static By ourPartners = By.XPath("/html/body/div[2]/nav/div/div/div[2]/div[1]/ul/li[2]/div/div/div/div/ul/li[3]/a");
        private static By ourLeadership = By.XPath("/html/body/div[2]/nav/div/div/div[2]/div[1]/ul/li[2]/div/div/div/div/ul/li[4]/a");
        private static By services = By.XPath("/html/body/div[2]/nav/div/div/div[2]/div[1]/ul/li[3]/span");
        private static By ourServices = By.XPath("/html/body/div[2]/nav/div/div/div[2]/div[1]/ul/li[3]/div/div/div/div/ul/li[1]/a");
        private static By ourWorkModel = By.XPath("/html/body/div[2]/nav/div/div/div[2]/div[1]/ul/li[3]/div/div/div/div/ul/li[2]/a");
        private static By careers = By.XPath("/html/body/div[2]/nav/div/div/div[2]/div[1]/ul/li[4]/span");
        private static By vacancies = By.XPath("/html/body/div[2]/nav/div/div/div[2]/div[1]/ul/li[4]/div/div/div/div/ul/li[1]/a");
        private static By workingAtStrypes = By.XPath("/html/body/div[2]/nav/div/div/div[2]/div[1]/ul/li[4]/div/div/div/div/ul/li[2]/a");
        private static By internship = By.XPath("/html/body/div[2]/nav/div/div/div[2]/div[1]/ul/li[4]/div/div/div/div/ul/li[3]/a");
        private static By contacts = By.XPath("/html/body/div[2]/nav/div/div/div[2]/div[1]/ul/li[6]/a");
        private static By resources = By.XPath("/html/body/div[2]/nav/div/div/div[2]/div[1]/ul/li[5]/span");
        private static By blog = By.XPath("/html/body/div[2]/nav/div/div/div[2]/div[1]/ul/li[5]/div/div/div/div/ul/li[1]/a");
        private static By whitepapers = By.XPath("/html/body/div[2]/nav/div/div/div[2]/div[1]/ul/li[5]/div/div/div/div/ul/li[2]/a");
        private static By videos = By.XPath("/html/body/div[2]/nav/div/div/div[2]/div[1]/ul/li[5]/div/div/div/div/ul/li[3]/a");
        private static By mediaKit = By.XPath("/html/body/div[2]/nav/div/div/div[2]/div[1]/ul/li[5]/div/div/div/div/ul/li[4]/a");
        private static By headerNearsurane = By.XPath("/html/body/div[2]/nav/div/div/div[2]/div[1]/ul/li[7]/a");
        private static By subscribe = By.XPath("/html/body/div[2]/nav/div/div/div[2]/div[2]/a/span");
        private static By getFreeCaseStudy = By.XPath("/html/body/div[3]/section[2]/div/div/div/div/p/a");
        private static By contentNearsurance = By.XPath("/html/body/div[3]/section[4]/div[2]/div/div/a");
        private static By consultancy = By.XPath("/html/body/div[3]/section[5]/div/div/div/div/div[2]/a/h3");
        private static By digitalTransoformation = By.XPath("/html/body/div[3]/section[5]/div/div/div/div/div[3]/a/h3");
        private static By applicationDevelopmentModernizationManagement = By.XPath("/html/body/div[3]/section[5]/div/div/div/div/div[4]/a/h3");
        private static By ITInfrastructure = By.XPath("/html/body/div[3]/section[5]/div/div/div/div/div[5]/a/h3");
        private static By SomeOfOurClients = By.XPath("/html/body/div[3]/section[9]/div/div/div[1]/p");
        private static By JoinTheTeamButton = By.XPath("/html/body/div[3]/section[12]/div/div/div[1]/div/a/span");
        private static By subscribeEmailInputField = By.XPath("/html/body/div[3]/section[13]/div/div/form/div/input[1]");
        private static By subscribeSubmitEmailButton = By.XPath("/html/body/div[3]/section[13]/div/div/form/div/button");
        private static By subscriptionPopUp = By.XPath("/html/body/div[8]/div/div");
        private static By facebook = By.XPath("/html/body/div[3]/footer/div/div[1]/div[1]/ul[1]/li[1]/a/i");
        private static By twitter = By.XPath("/html/body/div[3]/footer/div/div[1]/div[1]/ul[1]/li[2]/a/i");
        private static By youtube = By.XPath("/html/body/div[3]/footer/div/div[1]/div[1]/ul[1]/li[3]/a/i");
        private static By linkedin = By.XPath("/html/body/div[3]/footer/div/div[1]/div[1]/ul[1]/li[4]/a/i");
        private static By dismissCookieButton = By.XPath("/html/body/div[1]/div/a");
        private static By anchorToTop = By.XPath("/html/body/a[2]/i");
    }
}
