﻿using OpenQA.Selenium;
using System;

namespace StrypesTests.Util
{
    class Utils
    {
        internal static void ScrollTo(IWebDriver driver, int xPosition = 0, int yPosition = 0)
        {
            var js = String.Format("window.scrollTo({0}, {1})", xPosition, yPosition);
           ((IJavaScriptExecutor)driver).ExecuteScript(js);
        }

        internal static IWebElement ScrollToView(By selector, IWebDriver WebDriver)
        {
            var element = WebDriver.FindElement(selector);
            ScrollToView(element, WebDriver);
            return element;
        }

        internal static void ScrollToView(IWebElement element, IWebDriver driver)
        {
            if (element.Location.Y > 200)
            {
                ScrollTo(driver, 0, element.Location.Y - 100); //Make sure element is in the view but below the top navigation pane
            }
        }
    }
}
