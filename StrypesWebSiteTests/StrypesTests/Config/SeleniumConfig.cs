﻿namespace StrypesTests.Config
{
    class SeleniumConfig
    {
        public const string CHROME_OPTIONS = "disable-infobars";

        public const string CHROME_WEB_DRIVER_LOCATION = @".\..\..\..\drivers";
    }
}
