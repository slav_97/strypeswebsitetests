﻿namespace StrypesTests.Config
{
    class TestEmails
    {
        public static readonly string[] VALID_EMAILS = new string[] {"email@domain.com",
                                                                     "firstname.lastname@domain.com",
                                                                     "email@subdomain.domain.com",
                                                                     "firstname+lastname@domain.com",
                                                                     "email@123.123.123.123",
                                                                     "1234567890@domain.com",
                                                                     "email@domain-one.com",
                                                                     "_______@domain.com",
                                                                     "email@domain.name",
                                                                     "email@domain.co.jp",
                                                                     "email@111.222.333.44444",        
                                                                     "email@domain.web",                
                                                                     "firstname-lastname@domain.com"};


        public static readonly string[] INVALID_EMAILS = new string[] {"plainaddress",                    //Missing @ sign and domain
                                                                       "#@%^%#$@#$@#.com",	              //Garbage
                                                                       "@domain.com",                     //Missing username
                                                                       "Joe Smith <email@domain.com>",    //Encoded html within email is invalid
                                                                       "email.domain.com",                //Missing @
                                                                       "email@domain@domain.com",         //Two @ sign
                                                                       ".email@domain.com",               //Leading dot in address is not allowed
                                                                       "email.@domain.com",               //Trailing dot in address is not allowed
                                                                       "email@[123.123.123.123]",
                                                                       "\"email\"@domain.com",
                                                                       "email..email@domain.com",         //Multiple dots
                                                                       "あいうえお@domain.com",            //Unicode char as address
                                                                       "email@domain.com (Joe Smith)",    //Text followed email is not allowed
                                                                       "email@domain",                    //Missing top level domain (.com/.net/.org/etc)
                                                                       "email@-domain.com",               //Leading dash in front of domain is invalid
                                                                       "email@domain..com",               //Multiple dot in the domain portion is invalid
                                                                       "xxx@xxx.xxx' OR 1 = 1 LIMIT 1 -- ' ]"};              


    }
}
