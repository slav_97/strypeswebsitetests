﻿namespace StrypesTests.Config
{
    class WebsiteURLS
    {
        public const string HOME_PAGE_URL = "https://strypes.eu/";
        public const string ABOUT_US_URL = "https://strypes.eu/about/";
        public const string OUR_CLIENTS_URL = "https://strypes.eu/clients/";
        public const string OUR_PARTNERS_URL = "https://strypes.eu/partners/";
        public const string OUR_LEADERSHIP_URL = "https://strypes.eu/leadership/";
        public const string OUR_SERVICES_URL = "https://strypes.eu/services/";
        public const string OUR_WORK_MODEL_URL = "https://strypes.eu/services/our-model/";
        public const string VACANCIES_URL = "https://strypes.eu/careers/positions/";
        public const string WORKING_AT_STRYPES_URL = "https://strypes.eu/careers/work-environment/";
        public const string INTERNSHIP_URL = "https://strypes.eu/careers/internship/";
        public const string BLOG_URL = "https://strypes.eu/blog/";
        public const string WHITEPAPERS_URL = "https://strypes.eu/resources/whitepapers/";
        public const string VIDEOS_URL = "https://strypes.eu/resources/videos/";
        public const string MEDIA_KIT_URL = "https://strypes.eu/resources/media-kit/";
        public const string CONTACTS_URL = "https://strypes.eu/contacts/";
        public const string NEARSURANCE_URL = "https://strypes.eu/nearsurance/";
        public const string GET_YOUR_CASE_STUDY = "https://blog.strypes.eu/nearsurance-outsourcing-model?hsCtaTracking=5fa4d818-d4af-45ac-b37b-7fed63265998%7Cd4713726-e3de-40ce-825c-d321add0a5a3";
        public const string CONSULTANCY_URL = "https://strypes.eu/services/#consultancy";
        public const string DIGITAL_TRANSFORMATION_URL = "https://strypes.eu/services/#digital-transformation";
        public const string APPLICATIONS_DEVELOPMENT_MODERNIZATION_MANAGMENT_URL = "https://strypes.eu/services/#application-development";
        public const string IT_INFRASTRUCTURE_URL = "https://strypes.eu/services/#it-infrastructure";
        public const string STRYPES_BUSSINESS_EMAIL = "business@strypes.eu";
        public static readonly string[] BOARD_MEMBER_LINKEDINS = new string[] { "https://www.linkedin.com/in/todor-marinov-35b7b62a/", "https://www.linkedin.com/in/atanas-filchev-a038072a/", "https://www.linkedin.com/in/eric-damhuis-3758822/", "https://www.linkedin.com/in/jeroenvanhertum/" };
        public const string FACEBOOK_URL = "https://www.facebook.com/StrypesBulgaria/";
        public const string TWITTER_URL = "https://twitter.com/strypesICT";
        public const string YOUTUBE_URL = "https://www.youtube.com/channel/UC_bZ_Mu0G0OdQhcpsHq7w-w";
        public const string LINKEDIN_URL = "https://www.linkedin.com/company/strypes";
    }
}
