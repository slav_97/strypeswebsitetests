﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using StrypesTests.PageObjects;
using StrypesTests.Config;
using System.Threading;
using System.Collections.Generic;
using OpenQA.Selenium.Interactions;
using StrypesTests.Util;

namespace StrypesTests
{
    [TestClass]
    public class HomePageTests
    {
        IWebDriver Webdriver;

        [TestInitialize]
        public void TestInit()
        {
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.AddArgument(SeleniumConfig.CHROME_OPTIONS);
            Webdriver = new ChromeDriver(SeleniumConfig.CHROME_WEB_DRIVER_LOCATION, chromeOptions);
            Webdriver.Manage().Window.Maximize();
        }
        
        [TestMethod]
        [TestCategory("Header")]
        public void ClickingTheLogoTakesYouToHomePage()
        {
            HomePage hp = new HomePage(Webdriver);
            hp.goToPage();
            string homePageUrl = Webdriver.Url;
            hp.clickStrypesLogo();
            Assert.AreEqual(homePageUrl, Webdriver.Url);
        }

        [TestMethod]
        [TestCategory("Header")]
        public void ClickingHomeButtonTakesYouToHomePage()
        {
            HomePage hp = new HomePage(Webdriver);
            hp.goToPage();
            string homePageUrl = Webdriver.Url;
            hp.clickHomeButton();
            Assert.AreEqual(homePageUrl, Webdriver.Url);
        }

        [TestMethod]
        [TestCategory("Header")]
        public void ClickingTheAboutUsButtonTakesYouToAboutPage()
        {
            HomePage hp = new HomePage(Webdriver);
            hp.goToPage();
            hp.goToAboutUs();
            Assert.AreEqual(WebsiteURLS.ABOUT_US_URL, Webdriver.Url);
        }

        [TestMethod]
        [TestCategory("Header")]
        public void ClickingTheOurClientsButtonTakesYouToClientsPage()
        {
            HomePage hp = new HomePage(Webdriver);
            hp.goToPage();
            hp.goToOurClients();
            Assert.AreEqual(WebsiteURLS.OUR_CLIENTS_URL, Webdriver.Url);
        }
        
        [TestMethod]
        [TestCategory("Header")]
        public void ClickingTheOurPartnersButtonTakesYouToPartnersPage()
        {
            HomePage hp = new HomePage(Webdriver);
            hp.goToPage();
            hp.goToOurPartners();
            Assert.AreEqual(WebsiteURLS.OUR_PARTNERS_URL, Webdriver.Url);
        }

        [TestMethod]
        [TestCategory("Header")]
        public void ClickingTheOurLeadershipButtonTakesYouToLeadershipPage()
        {
            HomePage hp = new HomePage(Webdriver);
            hp.goToPage();
            hp.goToOurLeadersip();
            Assert.AreEqual(WebsiteURLS.OUR_LEADERSHIP_URL, Webdriver.Url);
        }

        [TestMethod]
        [TestCategory("Header")]
        public void ClickingTheOurServicesButtonTakesYouToServicesPage()
        {
            HomePage hp = new HomePage(Webdriver);
            hp.goToPage();
            hp.goToOurServices();
            Assert.AreEqual(WebsiteURLS.OUR_SERVICES_URL, Webdriver.Url);
        }

        [TestMethod]
        [TestCategory("Header")]
        public void ClickingTheOurWorkModelButtonTakesYouToWorkModelPage()
        {
            HomePage hp = new HomePage(Webdriver);
            hp.goToPage();
            hp.goToOurWorkModel();
            Assert.AreEqual(WebsiteURLS.OUR_WORK_MODEL_URL, Webdriver.Url);
        }

        [TestMethod]
        [TestCategory("Header")]
        public void ClickingTheVacanciesButtonTakesYouToVacanciesPage()
        {
            HomePage hp = new HomePage(Webdriver);
            hp.goToPage();
            hp.goToVacancies();
            Assert.AreEqual(WebsiteURLS.VACANCIES_URL, Webdriver.Url);
        }

        [TestMethod]
        [TestCategory("Header")]
        public void ClickingTheWorkingAtStrypesButtonTakesYouToWorkEnviromentPage()
        {
            HomePage hp = new HomePage(Webdriver);
            hp.goToPage();
            hp.goToWorkingAtStrypes();
            Assert.AreEqual(WebsiteURLS.WORKING_AT_STRYPES_URL, Webdriver.Url);
        }

        [TestMethod]
        [TestCategory("Header")]
        public void ClickingTheInternshipButtonTakesYouToInternshipPage()
        {
            HomePage hp = new HomePage(Webdriver);
            hp.goToPage();
            hp.goToInternship();
            Assert.AreEqual(WebsiteURLS.INTERNSHIP_URL, Webdriver.Url);
        }

        [TestMethod]
        [TestCategory("Header")]
        public void ClickingTheBlogButtonTakesYouToBlogPage()
        {
            HomePage hp = new HomePage(Webdriver);
            hp.goToPage();
            hp.goToBlog();
            Assert.AreEqual(WebsiteURLS.BLOG_URL, Webdriver.Url);
        }

        [TestMethod]
        [TestCategory("Header")]
        public void ClickingTheWhitepapersButtonTakesYouToWhitepapersPage()
        {
            HomePage hp = new HomePage(Webdriver);
            hp.goToPage();
            hp.goToWhitepapers();
            Assert.AreEqual(WebsiteURLS.WHITEPAPERS_URL, Webdriver.Url);
        }

        [TestMethod]
        [TestCategory("Header")]
        public void ClickingTheVideosButtonTakesYouToVideosPage()
        {
            HomePage hp = new HomePage(Webdriver);
            hp.goToPage();
            hp.goToVideos();
            Assert.AreEqual(WebsiteURLS.VIDEOS_URL, Webdriver.Url);
        }

        [TestMethod]
        [TestCategory("Header")]
        public void ClickingTheMediaKitButtonTakesYouToMediaPage()
        {
            HomePage hp = new HomePage(Webdriver);
            hp.goToPage();
            hp.goToMediaKit();
            Assert.AreEqual(WebsiteURLS.MEDIA_KIT_URL, Webdriver.Url);
        }

        [TestMethod]
        [TestCategory("Header")]
        public void ClickingTheContactsButtonTakesYouToContactsPage()
        {
            HomePage hp = new HomePage(Webdriver);
            hp.goToPage();
            hp.goToContacts();
            Assert.AreEqual(WebsiteURLS.CONTACTS_URL, Webdriver.Url);
        }

        [TestMethod]
        [TestCategory("Header")]
        public void ClickingTheNearsuranceButtonTakesYouToNearsurancePage()
        {
            HomePage hp = new HomePage(Webdriver);
            hp.goToPage();
            hp.goToHeaderNearsurance();
            Assert.AreEqual(WebsiteURLS.NEARSURANCE_URL, Webdriver.Url);
        }

        [TestMethod]
        [TestCategory("Header")]
        public void ClickingTheSubsribeButtonAddsAnAnchorToTheURLAndScrollsThePage()
        {
            HomePage hp = new HomePage(Webdriver);
            hp.goToPage();
            hp.clickSubscribe();
            Assert.AreEqual(WebsiteURLS.HOME_PAGE_URL + "#subscribe", Webdriver.Url);

            IJavaScriptExecutor js = (IJavaScriptExecutor)Webdriver;
            Thread.Sleep(3000);
            long scrollValue = (long)js.ExecuteScript("return window.pageYOffset;");
            Assert.AreNotEqual(0, scrollValue);
        }

        [TestMethod]
        [TestCategory("Content")]
        public void ClickingTheGetFreeCaseStudyButtonRedirectsToCaseSudyPage() 
        {
            HomePage hp = new HomePage(Webdriver);
            hp.goToPage();
            hp.clickGetYourFreeCaseStudy();
            Assert.AreEqual(WebsiteURLS.GET_YOUR_CASE_STUDY, Webdriver.Url);
        }

        [TestMethod]
        [TestCategory("Content")]
        public void ClickingTheNearsuranceButtonInTheContentRedirectsToNearsurancePage()
        {
            HomePage hp = new HomePage(Webdriver);
            hp.goToPage();
            
            hp.goToContentNearsurance();
            var browserTabs = Webdriver.WindowHandles;
            Webdriver.SwitchTo().Window(browserTabs[1]);
            Assert.AreEqual(WebsiteURLS.NEARSURANCE_URL, Webdriver.Url);
        }

        [TestMethod]
        [TestCategory("Our services section on home page")]
        public void ClickingTheConsultancyLinkRedirectsYouToTheConsultancyPage()
        {
            HomePage hp = new HomePage(Webdriver);
            hp.goToPage();
            hp.GoToConsultancy();
            Assert.AreEqual(WebsiteURLS.CONSULTANCY_URL, Webdriver.Url);
        }

        [TestMethod]
        [TestCategory("Our services section on home page")]
        public void ClickingTheDigitalInformationLinkTakesYouToDigitalInformationPage()
        {
            HomePage hp = new HomePage(Webdriver);
            hp.goToPage();
            hp.goToDigitalTransformation();
            Assert.AreEqual(WebsiteURLS.DIGITAL_TRANSFORMATION_URL, Webdriver.Url);
        }

        [TestMethod]
        [TestCategory("Our services section on home page")]
        public void ClickingTheAppDevModernizationAndManagementLinkTakesYouToCorrectPage()
        {
            HomePage hp = new HomePage(Webdriver);
            hp.goToPage();
            hp.goToApplicationDevelopmentModernizationManagement();
            Assert.AreEqual(WebsiteURLS.APPLICATIONS_DEVELOPMENT_MODERNIZATION_MANAGMENT_URL, Webdriver.Url);
        }

        [TestMethod]
        [TestCategory("Our services section on home page")]
        public void ClickingTheItInfrastructureLink()
        {
            HomePage hp = new HomePage(Webdriver);
            hp.goToPage();
            hp.goToItInfrastructure();
            Assert.AreEqual(WebsiteURLS.IT_INFRASTRUCTURE_URL, Webdriver.Url);
        }

        [TestMethod]
        [TestCategory("Our Board")]
        public void ClickingDifferentBoardMemberPicturesSwithcesTheCaroucel()
        {
            HomePage hp = new HomePage(Webdriver);
            hp.goToPage();
            hp.scrollToCarousel();

            hp.switchCarouselItem(1); 
            Assert.AreEqual(1, hp.getCarouselActiveItem());

            hp.switchCarouselItem(2);
            Assert.AreEqual(2, hp.getCarouselActiveItem());

            hp.switchCarouselItem(3);
            Assert.AreEqual(3, hp.getCarouselActiveItem());

            hp.switchCarouselItem(4);
            Assert.AreEqual(4, hp.getCarouselActiveItem());
        }

        [TestMethod]
        [TestCategory("Our Board")]
        public void CarouselEmailAndLinkedInForEachBoardMemberSendYouToTheRightPlace()
        {
            HomePage hp = new HomePage(Webdriver);
            hp.goToPage();
            hp.scrollToCarousel();
            for (int i = 1; i < 5; i++)
            {
                hp.switchCarouselItem(i);
                Thread.Sleep(1000);
                var emailElement =  Webdriver.FindElement(By.XPath("/html/body/div[3]/section[8]/div/div[2]/div/div/div[" + i + "]/div[2]/div/ul/li[1]/a"));
                Assert.AreEqual("mailto:" + WebsiteURLS.STRYPES_BUSSINESS_EMAIL, emailElement.GetAttribute("href"));
            }

            for (int i = 1; i < 5; i++)
            {
                hp.switchCarouselItem(i);
                Thread.Sleep(1000);
                Webdriver.FindElement(By.XPath("/html/body/div[3]/section[8]/div/div[2]/div/div/div[" + i + "]/div[2]/div/ul/li[2]/a")).Click();
                var browserTabs = Webdriver.WindowHandles;
                Webdriver.SwitchTo().Window(browserTabs[1]);
                Assert.AreEqual(WebsiteURLS.BOARD_MEMBER_LINKEDINS[i - 1], Webdriver.Url);
                Webdriver.Close();
                Webdriver.SwitchTo().Window(browserTabs[0]);
            }

        }

        [TestMethod]
        [TestCategory("Some of our clients")]
        public void EverySomeOfOurClientsLinkTakesYouToOurClients()
        {
            HomePage hp = new HomePage(Webdriver);
            hp.goToPage();
            hp.scrollDownToSomeOfOurClients();
            for (int i = 1; i < 4; i++)
            {
                Webdriver.FindElement(By.XPath("/html/body/div[3]/section[9]/div/div/div[2]/ul/li[" + i + "]/a/img")).Click();
                Assert.AreEqual(WebsiteURLS.OUR_CLIENTS_URL, Webdriver.Url);
                Webdriver.Navigate().Back();
            }
                
        }

        [TestMethod]
        [TestCategory("Join the team")]
        public void ClickingTheBeAPartOfTheTeamButtonWillSendYouToTheVacanciesPage()
        {
            HomePage hp = new HomePage(Webdriver);
            hp.goToPage();
            hp.pressTheJoinTheTeamButton();
            Assert.AreEqual(WebsiteURLS.VACANCIES_URL, Webdriver.Url);

        }

        [TestMethod]
        [TestCategory("Subscribe for emails")]
        public void EmailFieldValidEmailValidation()
        {
            List<Exception> errorList = new List<Exception>();
            HomePage hp = new HomePage(Webdriver);
            hp.goToPage();
            hp.scrollToEmailField();

            foreach (string email in TestEmails.VALID_EMAILS)
            {
                hp.enterAnEmailInTheSubscribeInputField(email);
                hp.submitSubscibtionEmail();
                Thread.Sleep(500);
                try
                {
                   if(hp.isRegistrationSuccessful() != 1)
                    {
                        throw new Exception("Registration failed with email: " + email + Environment.NewLine);
                    }
                }
                catch (Exception e)
                {
                    errorList.Add(e);
                }
                
                hp.waitForPopUpToDisapear();
                hp.clearEmailField();
            }

            if (errorList.Count > 0)
            {
                string combinedError = "";
                foreach (Exception e in errorList)
                {
                    combinedError += e.Message;
                }

                throw new Exception(combinedError);
            }
        }


        [TestMethod]
        [TestCategory("Subscribe for emails")]
        public void EmailFieldInvalidEmailValidation()
        {
            List<Exception> errorList = new List<Exception>();
            HomePage hp = new HomePage(Webdriver);
            hp.goToPage();
            hp.scrollToEmailField();

            foreach (string email in TestEmails.INVALID_EMAILS)
            {
                hp.enterAnEmailInTheSubscribeInputField(email);
                hp.submitSubscibtionEmail();
                Thread.Sleep(500);
                try
                {
                    if (hp.isRegistrationSuccessful() != -1)
                    {
                        throw new Exception("Registration succeeded with email: " + email + Environment.NewLine);
                    }
                }
                catch (Exception e)
                {
                    errorList.Add(e);
                }

                hp.waitForPopUpToDisapear();
                hp.clearEmailField();
            }

            if (errorList.Count > 0)
            {
                string combinedError = "";
                foreach (Exception e in errorList)
                {
                    combinedError += e.Message;
                }

                throw new Exception(combinedError);
            }
        }

        [TestMethod]
        [TestCategory("Footer")]
        public void SocialMediaLinksInFooterSendToCorrectPlace()
        {
            HomePage hp = new HomePage(Webdriver);
            var browserTabs = Webdriver.WindowHandles;
            hp.goToPage();
            hp.acceptCookies();
            hp.scrollToFooter();
            
            hp.goToFacebookPage();
            browserTabs = Webdriver.WindowHandles;
            Webdriver.SwitchTo().Window(browserTabs[1]);
            Assert.AreEqual(WebsiteURLS.FACEBOOK_URL, Webdriver.Url);
            Webdriver.Close();
            Webdriver.SwitchTo().Window(browserTabs[0]);

            hp.goToTwitterPage();
            browserTabs = Webdriver.WindowHandles;
            Webdriver.SwitchTo().Window(browserTabs[1]);
            Assert.AreEqual(WebsiteURLS.TWITTER_URL, Webdriver.Url);
            Webdriver.Close();
            Webdriver.SwitchTo().Window(browserTabs[0]);

            hp.goToYoutubePage();
            browserTabs = Webdriver.WindowHandles;
            Webdriver.SwitchTo().Window(browserTabs[1]);
            Assert.AreEqual(WebsiteURLS.YOUTUBE_URL, Webdriver.Url);
            Webdriver.Close();
            Webdriver.SwitchTo().Window(browserTabs[0]);

            hp.goToLinkedPage();
            browserTabs = Webdriver.WindowHandles;
            Webdriver.SwitchTo().Window(browserTabs[1]);
            Assert.AreEqual(WebsiteURLS.LINKEDIN_URL, Webdriver.Url);
            Webdriver.Close();
            Webdriver.SwitchTo().Window(browserTabs[0]);
        }

        [TestMethod]
        [TestCategory("Footer")]
        public void FooterStrypesURLSTakeYouToTheCorrectPages()
        {
            HomePage hp = new HomePage(Webdriver);
            var browserTabs = Webdriver.WindowHandles;
            hp.goToPage();
            hp.scrollToFooter();
            Thread.Sleep(4000);
            hp.acceptCookies();
            for (int i = 1; i < 6; i++)
            {
                Webdriver.FindElement(By.XPath("/html/body/div[3]/footer/div/div[1]/div[2]/ul/li["+ i + "]/a")).Click();
                Thread.Sleep(500);
                switch (i)
                {
                    case 1:
                        Assert.AreEqual(WebsiteURLS.HOME_PAGE_URL, Webdriver.Url);
                         hp.goToPage();
                         hp.scrollToFooter();
                         Thread.Sleep(1000);
                        break;
                    case 2:
                        hp.scrollToFooter();
                        Assert.AreEqual(WebsiteURLS.ABOUT_US_URL, Webdriver.Url);
                        Webdriver.Navigate().Back();
                        
                        break;
                    case 3:
                        hp.scrollToFooter();
                        Assert.AreEqual(WebsiteURLS.OUR_SERVICES_URL, Webdriver.Url);
                        Webdriver.Navigate().Back();
                        
                        break;
                    case 4:
                        hp.scrollToFooter();
                        Assert.AreEqual(WebsiteURLS.OUR_CLIENTS_URL, Webdriver.Url);
                        Webdriver.Navigate().Back();
                        
                        break;
                    case 5:
                        hp.scrollToFooter();
                        Thread.Sleep(1000);
                        Assert.AreEqual(WebsiteURLS.CONTACTS_URL, Webdriver.Url);
                        Webdriver.Navigate().Back();
                       
                        break;

                    default:
                        throw new IndexOutOfRangeException();
                }
            }

        }

        [TestMethod]
        [TestCategory("Footer")]
        public void FooterWorkEnviromentURLSTakeYouToTheCorrectPages()
        {
            HomePage hp = new HomePage(Webdriver);
            var browserTabs = Webdriver.WindowHandles;
            hp.goToPage();
            hp.scrollToFooter();
            Thread.Sleep(5000);
            hp.acceptCookies();
            for (int i = 1; i < 5; i++)
            {
                Thread.Sleep(1000);
                Webdriver.FindElement(By.XPath("/html/body/div[3]/footer/div/div[1]/div[3]/ul/li[" + i + "]/a")).Click();

                switch (i)
                {
                    case 1:
                        Assert.AreEqual(WebsiteURLS.WORKING_AT_STRYPES_URL, Webdriver.Url);
                        hp.goToPage();
                        hp.scrollToFooter();
                        Thread.Sleep(1000);
                        break;
                    case 2:
                        hp.scrollToFooter();
                        Assert.AreEqual(WebsiteURLS.INTERNSHIP_URL, Webdriver.Url);
                        Webdriver.Navigate().Back();

                        break;
                    case 3:
                        hp.scrollToFooter();
                        Assert.AreEqual(WebsiteURLS.VACANCIES_URL, Webdriver.Url);
                        Webdriver.Navigate().Back();

                        break;
                    case 4:
                        hp.scrollToFooter();
                        Assert.AreEqual(WebsiteURLS.VACANCIES_URL+ "#open-application", Webdriver.Url);
                        Webdriver.Navigate().Back();

                        break;
                   
                    default:
                        throw new IndexOutOfRangeException();
                }
            }

        }

        [TestMethod]
        [TestCategory("Footer")]
        public void FooterConnectURLSTakeYouToTheCorrectPages()
        {
            HomePage hp = new HomePage(Webdriver);
            var browserTabs = Webdriver.WindowHandles;
            hp.goToPage();
            hp.scrollToFooter();
            Thread.Sleep(5000);
            hp.acceptCookies();
            for (int i = 1; i < 5; i++)
            {
                
                Webdriver.FindElement(By.XPath("/html/body/div[3]/footer/div/div[1]/div[4]/ul/li[" + i + "]/a")).Click();
                Thread.Sleep(5000);
                switch (i)
                {
                    case 1:
                        browserTabs = Webdriver.WindowHandles;
                        Webdriver.SwitchTo().Window(browserTabs[1]);
                        Assert.AreEqual(WebsiteURLS.FACEBOOK_URL, Webdriver.Url);
                        Webdriver.Close();
                        Webdriver.SwitchTo().Window(browserTabs[0]);
                        break;
                    case 2:
                        browserTabs = Webdriver.WindowHandles;
                        Webdriver.SwitchTo().Window(browserTabs[1]);
                        Assert.AreEqual(WebsiteURLS.TWITTER_URL, Webdriver.Url);
                        Webdriver.Close();
                        Webdriver.SwitchTo().Window(browserTabs[0]);

                        break;
                    case 3:
                        browserTabs = Webdriver.WindowHandles;
                        Webdriver.SwitchTo().Window(browserTabs[1]);
                        Assert.AreEqual(WebsiteURLS.YOUTUBE_URL, Webdriver.Url);
                        Webdriver.Close();
                        Webdriver.SwitchTo().Window(browserTabs[0]);

                        break;
                    case 4:
                        browserTabs = Webdriver.WindowHandles;
                        Webdriver.SwitchTo().Window(browserTabs[1]);
                        Assert.AreEqual(WebsiteURLS.LINKEDIN_URL, Webdriver.Url);
                        Webdriver.Close();
                        Webdriver.SwitchTo().Window(browserTabs[0]);

                        break;

                    default:
                        throw new IndexOutOfRangeException();
                }
            }

        }

        [TestMethod]
        public void AnchorToTopOfPageIsVisibleAndWorks()
        {
            HomePage hp = new HomePage(Webdriver);
            hp.goToPage();
            hp.scrollToFooter();
            hp.useAnchorToTop();
            Thread.Sleep(2000);
            IJavaScriptExecutor executor = (IJavaScriptExecutor)Webdriver;
            long scrollPosition = (long)executor.ExecuteScript("return window.pageYOffset;");
            Assert.AreEqual(0, scrollPosition);
        }

        [TestCleanup]
        public void TestCleanup()
        {
            Webdriver.Quit();
        }
    }
}
